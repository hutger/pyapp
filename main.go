package main
import (
 "fmt"
 "log"
 "net/http"
)
func main() {
http.HandleFunc(
  "/hello",
  func(w http.ResponseWriter, r *http.Request) {
   fmt.Fprintf(w, "Hello World 3!")
  },
 )
log.Fatal(http.ListenAndServe(":8080", nil))
}
